Lập trình và sử dụng modul đọc thẻ RFID: "http://arduino.vn/bai-viet/833-lap-trinh-va-su-dung-modul-doc-rfid-rc522"

Lập trình xác nhận Card hợp lệ: "https://create.arduino.cc/projecthub/Aritro/security-access-using-rfid-reader-f7c746"

Giải thích và thêm data vào rfid tag: "https://www.electronicshub.org/write-data-to-rfid-card-using-rc522-rfid/"

Design flowchart: "https://drive.google.com/file/d/1XV0KuC6t7Iv8Unl2uF2671nQmj4vdX-w/view?usp=sharing"

Flowchart cụ thể cho từng chức năng: "https://drive.google.com/drive/folders/11y4MJm51QE4FynuzUmAMqcR25JK6wQLp?usp=sharing"

Tham khảo quy trình RFID ở thư viện: "https://idtvietnam.vn/en/ung-dung-cong-nghe-rfid-trong-quan-ly-va-tu-dong-hoa-thu-vien-571"
